from rest_framework import serializers
from .models import *

class DocumentationSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    
    class Meta:
        model = Documentation
        fields = '__all__'